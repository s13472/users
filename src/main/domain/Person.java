package main.domain;

import java.util.List;

public class Person {
    public Long id;
    public String firstName;
    public String lastName;
    public String phoneNumber;
    public List<Address> addresses;
    public List<Role> roles;
}
